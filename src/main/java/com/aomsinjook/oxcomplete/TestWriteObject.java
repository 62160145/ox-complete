
 

package com.aomsinjook.oxcomplete;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;





public class TestWriteObject {
    public static void main(String[] args){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null ;
        Player x = new Player('x');
        Player o = new Player('o');
        
        x.win();
        o.lose();
        x.draw();
        o.draw();
        System.err.println(x);
        System.err.println(o);
        try {
            file = new File("ox1.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(x);
            oos.writeObject(o);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                oos.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
            
            
            
            
            
            
            
            
}
